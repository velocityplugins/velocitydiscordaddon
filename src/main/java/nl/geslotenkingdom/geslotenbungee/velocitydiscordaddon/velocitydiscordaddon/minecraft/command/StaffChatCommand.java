package nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.minecraft.command;

import com.velocitypowered.api.command.Command;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.PlayerChatEvent;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.ServerConnection;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;
import net.kyori.text.serializer.legacy.LegacyComponentSerializer;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils.DiscordLoader;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils.PlayerToggleInfo;

import static nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.VelocityDiscordAddon.*;

public class StaffChatCommand implements Command {
    private static ProxyServer ps;

    public StaffChatCommand(ProxyServer ps) {
        this.ps = ps;
    }
    @Override
    public void execute(CommandSource source, String[] args) {
        if (source instanceof Player) {
            Player player = (Player) source;
            if (player.hasPermission("discord.staffchat")) {
                if (args.length == 0) {
                    if (PlayerToggleInfo.hasPlayerToToggle(player.getUniqueId())) {
                        PlayerToggleInfo.removePlayerFromToggle(player.getUniqueId());
                        sendToggleMessage(player, false);
                    } else {
                        PlayerToggleInfo.addPlayerToToggle(player.getUniqueId());
                        sendToggleMessage(player, true);
                    }
                } else {
                    sendStaffMessage(player, player.getCurrentServer().get(), String.join(" ", args));
                }
            } else {
                player.sendMessage(TextComponent.of("unfortunately you can't execute this command.").color(TextColor.RED));
            }
        } else {
            source.sendMessage(TextComponent.of("Only players can execute this command."));
        }
    }

    @Subscribe
    public void onChat(PlayerChatEvent event) {
        Player player = event.getPlayer();
        if (!PlayerToggleInfo.hasPlayerToToggle(player.getUniqueId())) {
            return;
        }

        event.setResult(PlayerChatEvent.ChatResult.denied());

        sendStaffMessage(player, player.getCurrentServer().get(), event.getMessage());
    }

    private void sendToggleMessage(Player player, boolean state) {
        player.sendMessage(color(toggleFormat.replace("{state}", state ? "enabled" : "disabled")));
    }

    private void sendStaffMessage(Player player, ServerConnection server, String message) {
        DiscordLoader.getJDA().getGuildById(discordid).getTextChannelById(staffcchannelid).sendMessage(discordmessageFormat.replace("{player}", player.getUsername())
                .replace("{server}", server != null ? server.getServerInfo().getName() : "N/A").replace("{message}", message)).queue();
        ps.getAllPlayers().stream().filter(target -> target.hasPermission("discord.staffchat")).forEach(target -> {
            target.sendMessage(color(minecraftmessageFormat.replace("{player}", player.getUsername()).replace("{message}", message)));
        });
    }

    private TextComponent color(String text) {
        return LegacyComponentSerializer.INSTANCE.deserialize(text, '&');
    }


}

