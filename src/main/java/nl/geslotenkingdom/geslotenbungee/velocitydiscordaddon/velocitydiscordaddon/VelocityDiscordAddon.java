package nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon;


import com.google.inject.Inject;
import com.moandjiezana.toml.Toml;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import lombok.Getter;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.discord.listener.DiscordStaffMessage;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.minecraft.command.StaffChatCommand;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.minecraft.command.ToggleChatCommand;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils.DiscordLoader;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils.PlayerToggleInfo;
import org.slf4j.Logger;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Plugin(id = "velocitydiscordaddon", name = "VelocityDiscordAddon", version = "1.0",
        description = "A plugin that has a little link to your discord server.", authors = {"TCOOfficiall"})

public final class VelocityDiscordAddon {

    @Inject
    @Getter
    public ProxyServer proxy;

    @Inject
    @Getter
    private Logger logger;

    @Inject
    @Getter
    @DataDirectory
    private Path configPath;

    public static String discordid;
    public static String discordmessageFormat;
    public static String minecraftmessageFormat;
    public static String toggleFormat;
    public static String staffcchannelid;
    public static String toggleFormat2;

    private Toml loadConfig(Path path) {
        File folder = path.toFile();
        File file = new File(folder, "config.toml");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        if (!file.exists()) {
            try (InputStream input = getClass().getResourceAsStream("/" + file.getName())) {
                if (input != null) {
                    Files.copy(input, file.toPath());
                } else {
                    file.createNewFile();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
                return null;
            }
        }

        return new Toml().read(file);
    }


    @Subscribe
    public void onProxyInitialize(ProxyInitializeEvent event) {

        Toml toml = loadConfig(configPath);
        if (toml == null) {
            logger.warn("Failed to load config.toml. Shutting down.");
            return;
        }
        try {
            String bottoken = toml.getString("bot-token");
            String ownerid = toml.getString("bot-owner-id");
            String prefix = toml.getString("bot-prefix");
            this.discordid = toml.getString("discord-id");
            this.staffcchannelid = toml.getString("staffchat-channel-id");
            this.discordmessageFormat = toml.getString("discord-message-format");
            this.toggleFormat = toml.getString("toggle-format");
            this.toggleFormat2 = toml.getString("toggle-format-chat");

            this.minecraftmessageFormat = toml.getString("minecraft-message-format");
            DiscordLoader.startBot(bottoken, prefix, ownerid);
            new PlayerToggleInfo();
            new DiscordStaffMessage(proxy);
        } catch (LoginException e) {
            e.printStackTrace();
            logger.error("Something whent wrong when setting up the bot, turning off VelocityDiscordAddon!");
        }

        StaffChatCommand scc = new StaffChatCommand(proxy);
        proxy.getCommandManager().register(scc, "discordstaffchat", "dsc");
        proxy.getEventManager().register(this, scc);

        proxy.getCommandManager().register(new ToggleChatCommand(), "discordtogglechat", "dtc");

        logger.info("Plugin has enabled!");
    }
}
