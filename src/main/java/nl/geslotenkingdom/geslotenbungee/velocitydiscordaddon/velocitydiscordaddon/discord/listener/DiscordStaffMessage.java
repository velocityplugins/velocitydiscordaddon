package nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.discord.listener;

import com.velocitypowered.api.proxy.ProxyServer;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.kyori.text.TextComponent;
import net.kyori.text.serializer.legacy.LegacyComponentSerializer;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils.PlayerToggleInfo;

import static nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.VelocityDiscordAddon.*;

public class DiscordStaffMessage extends ListenerAdapter {

    private static ProxyServer ps;

    public DiscordStaffMessage(ProxyServer ps) {
        this.ps = ps;
    }

    public DiscordStaffMessage() {
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor().getId().equals(event.getJDA().getSelfUser().getId())) {
            return;
        }

        if (event.getChannel().getId().equals(staffcchannelid)) {
            sendStaffMessage(event.getMessage().getContentRaw(), event);
        }
    }

    private void sendStaffMessage(String message, GuildMessageReceivedEvent event) {
        ps.getAllPlayers().stream().filter(target -> target.hasPermission("discord.staffchat")).forEach(target -> {
            {
                if (PlayerToggleInfo.hasPlayerToChat(target.getUniqueId())) {
                    target.sendMessage(color(minecraftmessageFormat.replace("{player}", event.getAuthor().getName()).replace("{message}", message)));
                }
            }
        });
    }

    private TextComponent color(String text) {
        return LegacyComponentSerializer.INSTANCE.deserialize(text, '&');
    }

}
