package nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.examples.command.PingCommand;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.discord.listener.DiscordStaffMessage;

import javax.security.auth.login.LoginException;

public class DiscordLoader {

    private static JDA jda;

    public static void startBot(String token, String prefix, String ownerid) throws LoginException {

        // define an eventwaiter, dont forget to add this to the JDABuilder!
        EventWaiter waiter = new EventWaiter();

        // define a command client
        CommandClientBuilder client = new CommandClientBuilder();

        // The default is "Type !!help" (or whatver prefix you set)
        client.useDefaultGame();

        // sets the owner of the bot
        client.setOwnerId(ownerid);

        // sets emojis used throughout the bot on successes, warnings, and failures
        client.setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26");

        // sets the bot prefix
        client.setPrefix(prefix);

        // adds commands
        client.addCommands(
                new PingCommand());


        // start getting a bot account set up
        jda = new JDABuilder(AccountType.BOT)
                // set the token
                .setToken(token)

                // set the game for when the bot is loading
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setGame(Game.playing("loading..."))

                // add the listeners
                .addEventListener(waiter)
                .addEventListener(client.build())
                .addEventListener(new DiscordStaffMessage())

                // start it up!
                .build();
    }

    public static JDA getJDA() {
        return jda;
    }

}
