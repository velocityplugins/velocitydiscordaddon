package nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.minecraft.command;

import com.velocitypowered.api.command.Command;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.PlayerChatEvent;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.ServerConnection;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;
import net.kyori.text.serializer.legacy.LegacyComponentSerializer;
import nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils.PlayerToggleInfo;

import static nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.VelocityDiscordAddon.*;

public class ToggleChatCommand implements Command {

    @Override
    public void execute(CommandSource source, String[] args) {
        if (source instanceof Player) {
            Player player = (Player) source;
            if (player.hasPermission("discord.staffchat")) {
                if (PlayerToggleInfo.hasPlayerToChat(player.getUniqueId())) {
                    PlayerToggleInfo.removePlayerFromChat(player.getUniqueId());
                    sendToggleMessage(player, false);
                } else {
                    PlayerToggleInfo.addPlayerToChat(player.getUniqueId());
                    sendToggleMessage(player, true);
                }
            } else {
                player.sendMessage(TextComponent.of("Unfortunately you can't execute this command.").color(TextColor.RED));
            }
        } else {
            source.sendMessage(TextComponent.of("Only players may execute this command."));
        }
    }

    private void sendToggleMessage(Player player, boolean state) {
        player.sendMessage(color(toggleFormat2.replace("{state}", state ? "enabled" : "disabled")));
    }
    private TextComponent color(String text) {
        return LegacyComponentSerializer.INSTANCE.deserialize(text, '&');
    }


}

