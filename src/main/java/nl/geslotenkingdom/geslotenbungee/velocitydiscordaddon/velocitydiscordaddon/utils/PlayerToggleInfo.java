package nl.geslotenkingdom.geslotenbungee.velocitydiscordaddon.velocitydiscordaddon.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class PlayerToggleInfo {



    private static Set <UUID> toggledPlayers;
    private static Set <UUID> chatenabledPlayers;

    public PlayerToggleInfo (){
        toggledPlayers = new HashSet<>();
        chatenabledPlayers = new HashSet<>();
    }


    public static void addPlayerToToggle(UUID u) {
        toggledPlayers.add(u);
    }
    public static void removePlayerFromToggle(UUID u) {
        toggledPlayers.remove(u);
    }
    public static boolean hasPlayerToToggle(UUID u) {
        return toggledPlayers.contains(u);
    }

    public static void addPlayerToChat(UUID u) {
        chatenabledPlayers.add(u);
    }
    public static void removePlayerFromChat(UUID u) {
        chatenabledPlayers.remove(u);
    }
    public static boolean hasPlayerToChat(UUID u) {
        return chatenabledPlayers.contains(u);
    }



}
